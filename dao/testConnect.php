<?php
    $dsn = 'mysql:host=localhost;dbname=test';
    $username = 'root';
    $password = '';
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );

    try {
        $dbh = new PDO($dsn, $username, $password, $options);
        echo "true connect";
    } catch (PDOException $e) {
        echo "not connect  <br>".$e->getMessage();
    }

    // close connection
    $dbh = null;
?>
